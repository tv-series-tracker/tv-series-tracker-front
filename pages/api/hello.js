// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  res.status(200).json({
    name: 'Tv Series Trakcer App',
    repository: "https://gitlab.com/tv-series-tracker/tv-series-tracker-front",
  })
}
